from django.db import models
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.


class Tasks(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField
    due_date = models.DateTimeField
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(on_delete=models.CASCADE)

    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True
    )


class Task(models.Model):
    assignee = models.ForeignKey(User, on_delete=models.CASCADE)

    def tasks_for_current_user(cls, current_user):
        return

    Task.objects.filter(assignee=tasks_for_current_user)
